import Vue from 'vue'
import Router from 'vue-router'
import Welcome from './views/Welcome.vue'
import Home from './views/Home.vue'
import Document from './views/Document.vue'
import Self from './views/Self.vue'
import Survey from './views/Survey.vue'
import TakeALesson from './views/TakeALesson.vue'
import Exams from './views/Exams.vue'

Vue.use(Router)
export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'welcome',
      component: Welcome
    },
    {
      path: '/home',
      name: 'home',
      component: Document
    },
    {
      path: '/document',
      name: 'document',
      component: Document
    },
    {
      path: '/self',
      name: 'self',
      component: Self
    },
    {
      path: '/survey',
      name: 'survey',
      component: Survey
    },
    {
      path: '/takeALesson',
      name: 'takeALesson',
      component: TakeALesson
    },
    {
      path: '/exams',
      name: 'exams',
      component: Exams
    }
  ]
})