import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {
  Welcome
} from './views/Welcome.vue'
import './registerServiceWorker'
import VueGoogleCharts from 'vue-google-charts'
Vue.component('app-welcome', Welcome)
Vue.config.productionTip = false

new Vue({
  VueGoogleCharts,
  router,
  render: h => h(App)
}).$mount('#app')